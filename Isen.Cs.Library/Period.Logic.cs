﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library
{
    public partial class Period
    {
        #region Constructeurs

        /// <summary>
        /// Constructeur début / fin.
        /// Les champs sont réordonnés
        /// </summary>
        /// <param name="start">Début</param>
        /// <param name="end">Fin</param>
        public Period(
            DateTime start,
            DateTime end)
        {
            if (start > end)
            {
                Start = end;
                End = start;
            }
            else
            {
                Start = start;
                End = end;
            }
        }

        /// <summary>
        /// Construire une période allant de la date donnée
        /// à la fin du même jour
        /// </summary>
        /// <param name="start"></param>
        public Period(DateTime start)
        {
            Start = start;
            End = start.LastMoment();
        }

        /// <summary>
        /// Construit une période allant de maintenant 
        /// à la fin de la journée.
        /// </summary>
        public Period() : this(DateTime.Now) { }

        /// <summary>
        /// Crée une période qui commence à start
        /// et dure la length donnée
        /// </summary>
        /// <param name="start"></param>
        /// <param name="length"></param>
        public Period(DateTime start, TimeSpan length)
            : this(start, start + length) { }

        /// <summary>
        /// Fin et durée
        /// </summary>
        /// <param name="length"></param>
        /// <param name="end"></param>
        public Period(TimeSpan length, DateTime end)
            : this(end, end - length) { }

        #endregion

        #region Factories

        /// <summary>
        /// Renvoie aujourd'hui
        /// de minuit à minuit -1ms
        /// </summary>
        public static Period Today 
            => new Period(DateTime.Today);

        /// <summary>
        /// Renvoie le week end courant si on
        /// est en week end, ou le suivant,
        /// si on est la semaine
        /// </summary>
        public static Period ThisWeekend
        {
            get
            {
                var now = DateTime.Now;
                var dw = now.DayOfWeek;
                DateTime start;
                DateTime end;
                switch (dw)
                {
                    case DayOfWeek.Saturday:
                        start = now.Date;
                        break;
                    case DayOfWeek.Sunday:
                        start = now.Date.AddDays(-1);
                        break;
                    default:
                        start = now.Next(DayOfWeek.Saturday);
                        break;
                }
                end = start.AddDays(1).LastMoment();
                return new Period(start, end);
            }
        }

        public static Period Random
        {
            get
            {
                var seed = Guid.NewGuid().GetHashCode();
                var rnd = new Random(seed);
                var offset = rnd.Next(-1000, 1000);
                var offsetMinutes = rnd.Next(0, 1440);
                var length = rnd.Next(10, 144_000);
                return new Period(
                    DateTime.Now
                        .AddDays(offset)
                        .AddMinutes(offsetMinutes),
                    TimeSpan.FromMinutes(length));
            }
        }

        #endregion

        #region Inclusions

        //-------S----------E--------
        //-------|-----d----|-------- > true
        //---d---|----------|-------- > false
        //-------|----------|-----d-- > false
        public bool Contains(DateTime date)
            => Start <= date && date <= End;

        //-------S----------E--------
        //-------|---s--e---|-------- > true
        //-------|---s------|---e---- > false
        //--s----|----------|---e---- > false
        //---s---|------e---|-------- > false
        //---s--e|----------|-------- > false
        public bool Contains(Period other)
            => Start <= other.Start && other.End <= End;

        //-------S----------E--------
        //-------|---s--e---|-------- > true
        //-------|---s------|---e---- > true
        //--s----|----------|---e---- > true
        //---s---|------e---|-------- > true
        //---s--e|----------|-------- > false
        //-------|----------|-s---e-- > false
        public bool Intersects(Period other)
            => Start <= other.End &&
                other.Start <= End;

        #endregion

        public override string ToString()
        {
            var durationInHours = (int) Math.Round(Length.TotalHours);
            var durationInDays = (int) Math.Round(Length.TotalDays);
            var durationInYears = (int) Math.Round(Length.TotalDays / 365.25);
            var durationText = "";
            if (durationInHours < 24)
                durationText = $"{durationInHours}h";
            else if (durationInDays < 365)
                durationText = $"{durationInDays}j";
            else durationText = $"{durationInYears} an(s)";

            return $"{Start:d} {Start:t} " +
                   $"=> {End:d} {End:t} ({durationText})";
        }
    }
}
