﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library
{
    public class MyCollection<T>
        : IMyCollection<T>
    {
        // Tableau de persistance de la collection
        private T[] _values;

        // Ctor
        public MyCollection()
        {
            _values = new T[0];
        }

        /// <inheritdoc />
        public int Count =>  _values.Length;
        /// <inheritdoc />
        public T[] Values => _values;
        /// <inheritdoc />
        public T this[int index]
        {
            get => _values[index];
            set => _values[index] = value;
        }
        /// <inheritdoc />
        public void Add(T item)
        {
            Insert(Count, item);
        }
        /// <inheritdoc />
        public void Insert(int index, T item)
        {
            // Tester les limites d'index
            if (index < 0 || index > Count)
                throw new IndexOutOfRangeException();
            // tableau temporaire de taille taille+1
            var tmp = new T[Count + 1];
            // parcourir avant la césure
            for (var i = 0; i < index; i++) tmp[i] = _values[i];
            // A la césure : nouvelle valeur
            tmp[index] = item;
            // Après la césure : affectation décalée
            for (var i = index + 1; i < tmp.Length; i++) tmp[i] = _values[i - 1];

            // Réaffecter le tableau
            _values = tmp;
        }
        /// <inheritdoc />
        public void RemoveAt(int index)
        {
            // Tester les limites d'index
            if (index < 0 || index >= Count)
                throw new IndexOutOfRangeException();
            // tableau temporaire de taille taille+1
            var tmp = new T[Count - 1];

            // parcourir
            for (var i = 0; i < _values.Length; i++)
            {
                if (i < index) tmp[i] = _values[i];
                else if (i > index) tmp[i - 1] = _values[i];
            }

            // Réaffecter le tableau
            _values = tmp;
        }
        /// <inheritdoc />
        public int IndexOf(T item)
        {
            for (var i = 0; i < _values.Length; i++)
                if (_values[i].Equals(item)) return i;
            return -1;
        }
        /// <inheritdoc />
        public bool Contains(T item)
            => IndexOf(item) >= 0;
        /// <inheritdoc />
        public bool Remove(T item)
        {
            var index = IndexOf(item);
            if (index <= 0) return false;
            RemoveAt(index);
            return true;
        }
        /// <inheritdoc />
        public void Clear()
        {
            _values = new T[0];
        }
        /// <inheritdoc />
        public IEnumerator<T> GetEnumerator()
        {
            for (var i = 0; i < Count; i++)
                yield return _values[i];
        }

        /// <inheritdoc />
        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var item in _values)
                sb.Append($"{item}\t");
            return sb.ToString();
        }
    }

    #region Explication des propriétés, getter, setter
    //public void Test()
    //{
    //    var mc = new MyClass();
    //    var i = mc.MyInt;
    //    mc.MyInt = 12;
    //}

    //public class MyClass
    //{
    //    private int _myInt;
    //    // getter / setter #c# explicite
    //    public int MyInt
    //    {
    //        get { return _myInt > 0 ? _myInt : 0; }
    //        private set { _myInt = value; }
    //    }

    //    // get / set implicite
    //    public string MyString { get; set; }

    //    // getter / setter #java
    //    public int getMyInt()
    //    {
    //        return _myInt;
    //    }
    //    private void setMyInt(int value)
    //    {
    //        _myInt = value;
    //    }
    //}

    #endregion
}
