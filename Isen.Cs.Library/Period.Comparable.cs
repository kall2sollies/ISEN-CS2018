﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.Cs.Library.Lessons;

namespace Isen.Cs.Library
{
    public partial class Period : IComparable<Period>
    {
        // comparaison chronologique
        public bool IsGreaterThan(Period other)
        {
            if (other == null) return true;
            var startGreater = Start > other.Start;
            var startEquals = Start == other.Start;
            var endGreater = End > other.End;

            return startGreater ||
                (startEquals && endGreater);
            // Ce code fournit un tri basé sur la durée
            //return Length > other.Length;
        }

        public int CompareTo(Period other)
        {
            if (other == null) return 1;
            if (IsGreaterThan(other)) return 1;
            if (this == other) return 0;
            return -1;
        }

        public static int Compare(Period left, Period right)
        {
            if (ReferenceEquals(left, right)) return 0;
            if (ReferenceEquals(left, null)) return 1;
            return left.CompareTo(right);
        }

        public static bool operator <(Period left, Period right)
            => Compare(left, right) < 0;

        public static bool operator <=(Period left, Period right)
            => Compare(left, right) <= 0;

        public static bool operator >(Period left, Period right)
            => Compare(left, right) > 0;

        public static bool operator >=(Period left, Period right)
            => Compare(left, right) >= 0;
    }
}
