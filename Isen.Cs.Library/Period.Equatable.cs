﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library
{
    public partial class Period : IEquatable<Period>
    {
        // Issu de IEquatable<T>
        public bool Equals(Period other)
        {
            // caster en object afin d'appeler 
            // l'opérateur == sur object et non
            // sur Period
            if ((object)other == null) return false;
            return Start == other.Start
                   && End == other.End;
        }

        // override de Object.Equals
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (!(obj is Period)) return false;
            return Equals((Period)obj);
        }

        // override opérateur ==
        public static bool operator ==(Period a, Period b)
        {
            if (ReferenceEquals(a, b)) return true;
            if ((object)a == null || (object)b == null)
                return false;
            return a.Equals(b);
        }

        // override opérateur !=
        public static bool operator !=(Period a, Period b)
        {
            return !(a == b);
        }

        // par défaut, un même hash = une même référence
        // l'override permet de dire :
        // même hash = mêmes dates
        public override int GetHashCode()
        {
            var hash = 17;
            hash = hash * 23 + Start.GetHashCode();
            hash = hash * 23 + End.GetHashCode();
            return hash;
        }
    }
}
