﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Isen.Cs.Library
{
    public class MyThreads
    {
        // démarrage du process
        private static DateTime _start;
        // générateur aléatoire
        private readonly Random _rnd;
        // threadsafe lock
        private static object _lock = new object();

        public MyThreads()
        {
           _rnd = new Random(Guid.NewGuid().GetHashCode());
        }

        public void Run()
        {
            _start = DateTime.Now;
            Log($"Main thread starts on thread {Thread.CurrentThread.ManagedThreadId}");

            // exécution asynchrone
            Task.Run(() =>
            {
                LongTask("A", ConsoleColor.DarkYellow);
                LongTask("B", ConsoleColor.DarkCyan);
                LongTask("C", ConsoleColor.DarkRed);
            });

            // Parallèle
            Task.Run(() => LongTask("PA", ConsoleColor.Blue));
            Task.Run(() => LongTask("PB", ConsoleColor.Cyan));
            Task.Run(() => LongTask("PC", ConsoleColor.DarkMagenta));

            // Lance la loop d'echo (bloque le thread principal)
            EchoLoop();

            // Cette ligne est atteinte alors que les tâches ne sont pas terminées
            Log("Main thread returns");
            Console.ReadLine();
        }

        private void EchoLoop()
        {
            Log($"EchoLoop thread starts " +
                $"on thread {Thread.CurrentThread.ManagedThreadId}", 1, ConsoleColor.Green);

            ConsoleKeyInfo key;
            do
            {
                // Eviter de faire trop d'itérations quand le user ne tape pas
                while(!Console.KeyAvailable) Thread.Sleep(100);
                key = Console.ReadKey(true);
                Log($"EchoLoop says user typed on {key.Key}", 1, ConsoleColor.Green);
                OnKeyPressed(key.Key);
            } while (key.Key != ConsoleKey.Escape);

            Log($"EchoLoop thread returns", 1, ConsoleColor.Green);
        }

        public void LongTask(string name, ConsoleColor color)
        {
            Log($"LongTask {name} thread starts " +
                $"on thread {Thread.CurrentThread.ManagedThreadId}", 1, color);

            var loops = _rnd.Next(3, 10);
            for (var i = 1; i <= loops; i++)
            {
                var waitFor = _rnd.Next(200, 1000);
                var progress = (int) ((double)i / (double)loops * 100d);
                Log($"Task {name} : {progress}%", 2, color);
                Thread.Sleep(waitFor);
            }

            Log($"LongTask {name} returns", 1, color);
        }

        public static void Log(string text, int depth = 0, ConsoleColor color = ConsoleColor.Gray)
        {
            lock(_lock) // empêche que plusieurs threads ne rentrernt en même temps
            {
                // changer la couleur si elle a été précisée
                if (color != ConsoleColor.Gray) Console.ForegroundColor = color;
                // calcul de l'indentation et du temps écoulé
                var indent = "".PadLeft(depth * 2, ' ');
                var elasped = ((int) (DateTime.Now - _start).TotalMilliseconds)
                    .ToString().PadLeft(5, '0');
                // log
                Console.WriteLine($"[+{elasped}] {indent}{text}");
                // reset de la couleur
                Console.ForegroundColor = ConsoleColor.Gray;
            }
        }

        // Méthode pour lancer un événement
        public void OnKeyPressed(ConsoleKey key)
        {
            var arg = new KeyPressedEventArgs() { KeyPressed = key };
            if (KeyPressed != null) KeyPressed(arg);
        }


        // Un membre du type du delegate
        public event KeyPressedEventHandler KeyPressed;
    }

    public class KeyPressedEventArgs : EventArgs
    {
        public ConsoleKey KeyPressed { get; set; }
    }

    public delegate void KeyPressedEventHandler(KeyPressedEventArgs args);
}
