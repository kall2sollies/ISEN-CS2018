﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library.Lessons
{
    public class G_Lambdas
    {
        
        public static void Run()
        {
            OldSyntaxDelegate();
            OldSyntaxDelegateWithLambdas();
            PureLambdas();
        }

        public static void OldSyntaxDelegate()
        {
            // Ancienne syntaxe : delegate
            Operation currentOperation;
            // Affecter une action
            MyAction action = delegate(string text)
            {
                Console.WriteLine(text);
            };
            // Affectation d'une function anonyme à la fonction
            currentOperation = delegate (int a, int b)
            {
                return a + b;
            };

            action($"2+3={currentOperation(2, 3)}");

            // Reaffecter une autre fonction anonyme
            currentOperation = delegate (int a, int b)
            {
                return a - b;
            };
            action($"2-3={currentOperation(2, 3)}");
        }

        public static void OldSyntaxDelegateWithLambdas()
        {
            // Lambda d'une fonction
            Operation currentOperation = (a, b) => a + b;
            // Lambda d'une action
            MyAction action = (text) => Console.WriteLine(text);
            // usage
            action($"2+3={currentOperation(2,3)}");
            // réaffectation
            currentOperation = (a, b) => a - b;
            action($"2-3={currentOperation(2, 3)}");
        }

        public static void PureLambdas()
        {
            // Lambda d'une fonction. Plus besoin de déclarer un delegate hors classe
            Func<int, int, int> currentOperation = (a, b) => a + b;
            // Lambda d'une action
            Action<string> action = (text) => Console.WriteLine(text);
            // usage
            action($"2+3={currentOperation(2, 3)}");
            // réaffectation
            currentOperation = (a, b) => a - b;
            action($"2-3={currentOperation(2, 3)}");
        }
    }
    // Déclaration d'un delegate > une signature de fonction (avec un retour)
    public delegate int Operation(int a, int b);
    // déclaration d'un delegate > une signature d'action (fonction qui return void)
    public delegate void MyAction(string text);
}
