﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library.Lessons
{
    public class D_MyCollection
    {
        public static void Run()
        {
            IMyCollection<string> myCollection = 
                new MyCollection<string>();
            Console.WriteLine("====ADD====");
            myCollection.Add("10");
            myCollection.Add("20");
            myCollection.Add("30");
            myCollection.Add("40");
            Console.WriteLine(myCollection);
            // 10 20 30 40
            Console.WriteLine("====INSERT====");
            myCollection.Insert(2, "25");
            Console.WriteLine(myCollection);
            // 10 20 25 30 40
            myCollection.Insert(0, "5");
            Console.WriteLine(myCollection);
            // 5 10 20 25 30 40
            myCollection.Insert(6, "45");
            Console.WriteLine(myCollection);
            // 5 10 20 25 30 40 45
            Console.WriteLine("====REMOVE====");
            myCollection.RemoveAt(3);
            Console.WriteLine(myCollection);
            // 5 10 20 30 40 45
            myCollection.RemoveAt(0);
            Console.WriteLine(myCollection);
            // 10 20 30 40 45
            myCollection.RemoveAt(4);
            Console.WriteLine(myCollection);
            // 10 20 30 40
            Console.WriteLine("====INDEXER====");
            var element2 = myCollection[2];
            Console.WriteLine(element2);
            // 30
            myCollection[3] = "42";
            Console.WriteLine(myCollection);
            // 10 20 30 42
            var indexOf42 = myCollection.IndexOf("42");
            Console.WriteLine(indexOf42);
            // 3
            var contains30 = myCollection.Contains("30");
            Console.WriteLine(contains30); 
            // True
            myCollection.Remove("20");
            Console.WriteLine(myCollection);
            // 10 30 42
            myCollection.Clear();
            myCollection.Add("bonjour");
            myCollection.Add("hi");
            myCollection.Add("hallo");
            Console.WriteLine(myCollection);
            // bonjour hi hallo
            foreach (var item in myCollection)
            {
                Console.WriteLine(item);
            }
        }
    }
}
