﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Isen.Cs.Library.Lessons
{
    public class E_DateTime
    {
        public static void Run()
        {
            // Maintenant
            var now = DateTime.Now;
            Console.WriteLine(now);
            Console.WriteLine($"Nous sommes le {now:dddd d MMMM yyyy} et il est {now:HH:mm}");
            // Est-ce qu'on est vendredi ?
            // Et comment on met des literal de string dans la string interpolée
            Console.WriteLine(
                $"C'est vendredi ? {(now.DayOfWeek == DayOfWeek.Friday ? "Oui" : "Non")}");
            // C'est quand les prochains 1er mai où on a pas de bol (mode bourrin)
            var numberOfDates = 30;
            for (var current = now; current < DateTime.MaxValue; current = current.AddDays(1))
            {
                if ((current.DayOfWeek == DayOfWeek.Sunday || current.DayOfWeek == DayOfWeek.Saturday)
                    && current.Month == 5 
                    && current.Day == 1)
                {
                    Console.WriteLine($"Le prochain samedi dimanche 1/5 c'est en {current.Year}");
                    numberOfDates--;
                    if (numberOfDates == 0) break;
                }
            }
            // Liste des bissextiles depuis le 1/1/1
            var start = new DateTime(999,1,1);
            var yearIncrement = 1;
            var loopCount = 0;
            var leapYearCount = 0;
            for (var current = start; current.Year <= 2020; current = current.AddYears(yearIncrement))
            {
                if (current.IsBissextile())
                {
                    leapYearCount++;
                    yearIncrement = 4;
                    Console.WriteLine($"{current.Year} est bissextile");
                }
                loopCount++;
            }
            Console.WriteLine($"Count : {leapYearCount} Loopcount : {loopCount}");

            // Calendrier julien
            Console.WriteLine(new DateTime(1582, 10, 4, new JulianCalendar()));
            // 14/10/1582

            // Durées
            // prochain noel (ne pas exécuter ce code entre le 2612 et le 3112 !
            var nextChristmas = new DateTime(now.Year, 12,25);
            TimeSpan duree = nextChristmas - now;
            Console.WriteLine($"Prochain noel dans {duree.TotalDays} jours");
        }
    }
}
