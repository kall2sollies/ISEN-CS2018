﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Isen.Cs.Library.Lessons
{
    public class H_LinQ
    {
        public static void Run()
        {
            // Générer 100 périodes
            const int NB_PERIODS = 105;
            var periods = new List<Period>();
            for(var i = 0 ; i < NB_PERIODS; i++) periods.Add(Period.Random);
            
            // Créer une liste des périodes qui durent moins de n jour
            // en utilisant une fonction de filtrage déclarée en lambda
            Func<Period, int, bool> filter = (period, days) 
                => period.Length < TimeSpan.FromDays(days);
            var filteredList = new List<Period>();
            foreach (var period in periods)
            {
                if (filter(period, 7)) filteredList.Add(period);
            }
            Console.WriteLine($"{filteredList.Count} / {NB_PERIODS} durent moins de 7 jours");

            //=======================================
            // =============WHERE====================
            //=======================================

            // Pareil, avec l'API LinQ
            var listFilteredByLinq = periods
                .Where(period => period.Length < TimeSpan.FromDays(7))
                .ToList();
            Console.WriteLine($"LINQ : {listFilteredByLinq.Count} / {NB_PERIODS} durent moins de 7 jours");
            
            // Périodes qui commencent une lundi
            var mondayPeriods = periods
                .Where(p => p.Start.DayOfWeek == DayOfWeek.Monday)
                .ToList();
            Console.WriteLine($"LINQ : {mondayPeriods.Count} / {NB_PERIODS} commencent un lundi");

            // Séparer les appels à Where
            // (pour par exemple faire des filtres conditionnés à un test)
            var mondayPeriodsQuery = periods.AsQueryable();
            // je peux filtrer ou non selon le résultat d'un test
            if(true) mondayPeriodsQuery = mondayPeriodsQuery
                .Where(p => p.Start.DayOfWeek == DayOfWeek.Monday);
            if (true) mondayPeriodsQuery = mondayPeriodsQuery
                .Where(p => p.End.DayOfWeek == DayOfWeek.Monday);
            // Générer la liste
            var mondayPeriods2 = mondayPeriodsQuery.ToList();
            Console.WriteLine($"LINQ : {mondayPeriods2.Count} / {NB_PERIODS} commencent et finissent un lundi");

            //=======================================
            // =============COUNT====================
            //=======================================
            var nbMondayPeriods = periods.Count(p => p.Start.DayOfWeek == DayOfWeek.Monday);
            Console.WriteLine($"LINQ : {nbMondayPeriods} / {NB_PERIODS} commencent un lundi");

            //=======================================
            // =============SELECT / ORDER===================
            //=======================================
            var dureesFromList = periods
                // Filtrage
                .Where(p => p.Length > TimeSpan.FromDays(1))
                // Sélection du champ Period.Length (TimeSpan)
                .Select(p => p.Length)
                // Trier selon le champ TimeSpan.TotalDays
                .OrderBy(ts => ts.TotalDays)
                // Sélectionner afin de convertir le Timespan en nb de jours entier
                .Select(ts => (int)ts.TotalDays)
                // Eliminer les doublons
                .Distinct()
                // Réaliser l'énumération
                .ToList();
            //foreach(var duree in dureesFromList) Console.WriteLine($"Durée: {duree} jours");

            //=======================================
            // =============Syntaxe alternative======
            //=======================================
            var dureesFromListSQL =
                (from p in periods
                where p.Length > TimeSpan.FromDays(1)
                orderby p.Length
                select (int) p.Length.TotalDays).Distinct();

            //=======================================
            // =============First(OrDefault)======
            //=======================================
            var first = periods
                .FirstOrDefault(p => p.Start.Day == 1 && p.Start.Month == 1);
            Console.WriteLine($"La 1ere période qui commence un 1er janvier commence le " +
                              $"{first?.Start.ToString() ?? "NULL"}");
            // ?. Null propagation (navigue vers la propriété si le parent est non null)
            // ?? Coalesce : renvoie le 1er non null

            //=======================================
            // =============Nullables================
            //=======================================

            // DateTime? = Nullable<DateTime>
            DateTime? firstDt = first?.Start;
            // Tous les types primitifs et les types valeurs ont un Nullable<> 
            // en faisant suivre leur nom d'un ?
            bool trueOrFalse = true;
            bool? quatumBool = null;
            int? nullInt = null;
            if (nullInt.HasValue)
            {
                var i = nullInt.Value;
                var ii = nullInt.GetValueOrDefault(0);
                var iii = nullInt ?? 0;
            }
            Nullable<int> nullInt2 = nullInt;


            //=======================================
            // ========GroupBy / ToLookup============
            //=======================================
            // Compter le nb de périodes par jour de semaine du début
            // => Faire des groupes
            // Générer les sous-listes de périodes, regroupées par
            // Le nom du jour de leur champ Start
            var periodsByDayOfWeek = periods
                .ToLookup(p => p.Start.DayOfWeek);
            // Itérer sur les groupes
            foreach (var group in periodsByDayOfWeek)
            {
                Console.WriteLine($"{group.Key} : {group.Count()} périodes");
            }
            Console.WriteLine("--------------");
            // Groupes dans l'ordre
            var keys = periodsByDayOfWeek // type = ILookup
                // Projecter les clés de regroupement (DayOfWeek) dans une liste
                .Select(p => p.Key)
                // trier cette liste par l'entier correspondant à l'enum DayOfWeek
                // Afin d'avoir les jours dans l'ordre
                .OrderBy(k => (int)k);
            // Itérer sur les clés (DayOfWeek)
            foreach (var key in keys)
            {
                // Un ILookup permet de récup les valeurs en donnant l'index du groupe
                var values = periodsByDayOfWeek[key];
                Console.WriteLine($"{key} : {values.Count()} périodes");
            }

            //=======================================
            // ========Skip / Take===================
            //=======================================
            var orderedPeriods = periods
                .OrderBy(p => p.Start)
                .ThenBy(p => p.End)
                .ToList();

            var pageSize = 10;
            for (var currentPageIndex = 0; 
                currentPageIndex < (int)(orderedPeriods.Count / pageSize); 
                currentPageIndex++)
            {
                var currentPage = orderedPeriods
                    .Skip(currentPageIndex * pageSize)
                    .Take(pageSize);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"Page {currentPageIndex + 1}");
                Console.ForegroundColor = ConsoleColor.Gray;
                foreach(var p in currentPage) Console.WriteLine($"\t{p}");
            }

            var currentMemory = GC.GetTotalMemory(true) / 1024;
            Console.WriteLine($"Total memory usage : {currentMemory} ko");
        }
    }
}
