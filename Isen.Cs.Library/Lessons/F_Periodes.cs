﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library.Lessons
{
    public class F_Periodes
    {
        public static void Run()
        {
            var summer = new Period(
                new DateTime(2018,7,1),
                new DateTime(2018,8,31));
            var myself = new Period(
                new DateTime(1980,2,28),
                DateTime.Now);
            var oneParam = new Period(
                new DateTime(1824,4,5,13,45,12));
            var restOfTheDay = new Period();

            Console.WriteLine(summer);
            Console.WriteLine(myself);
            Console.WriteLine(oneParam);
            Console.WriteLine(restOfTheDay);

            //var periodByFields = new Period();
            //periodByFields.Start = new DateTime(1980, 2, 28);
            //periodByFields.End = new DateTime(1985, 5, 22);
            //Console.WriteLine(periodByFields);

            // autre syntaxe d'initialisation d'instance
            //var periodWithFieldsInitializers = new Period()
            //{
            //    Start = new DateTime(1985, 5, 22),
            //    End = new DateTime(1980, 2, 28)
            //};
            //Console.WriteLine(periodWithFieldsInitializers);

            // syntaxe des paramètres nommés
            // (valable pour toute méthode)
            // On peut les passer dans l'ordre qu'on veut
            var namedParameters = new Period(
                end: DateTime.MinValue, 
                start: DateTime.Now);
            Console.WriteLine(namedParameters);

            var startAndLength = new Period(
                start: DateTime.Now, 
                length: TimeSpan.FromDays(7));
            Console.WriteLine(startAndLength);

            var endAndLength = new Period(
                end: DateTime.Now,
                length: TimeSpan.FromDays(7));
            Console.WriteLine(endAndLength);

            var today = Period.Today;
            Console.WriteLine(today);

            var nextWed = DateTime.Now.Next(DayOfWeek.Wednesday);
            var nextSat = DateTime.Now.Next(DayOfWeek.Saturday);
            Console.WriteLine(nextWed);
            Console.WriteLine(nextSat);

            var thisWeekend = Period.ThisWeekend;
            Console.WriteLine(thisWeekend);
            Console.WriteLine(
                "thisWeekend contains nextSat ? " +
                $"{thisWeekend.Contains(nextSat)}");
            Console.WriteLine(
                 "thisWeekend contains now ? " +
                $"{thisWeekend.Contains(DateTime.Now)}");

            Console.WriteLine(
                "thisWeekend contains thisWeekend ? " +
                $"{thisWeekend.Contains(thisWeekend)}");
            Console.WriteLine(
                "thisWeekend contains today (p) ? " +
                $"{thisWeekend.Contains(Period.Today)}");
            Console.WriteLine(
                "thisWeekend contains nextSat (p) ? " +
                $"{thisWeekend.Contains(new Period(nextSat))}");

            Console.WriteLine(
                $"thisWeekend hash = {thisWeekend.GetHashCode()}");
            var thisWeekendSameRef = thisWeekend;
            Console.WriteLine(
                $"thisWeekend hash = {thisWeekendSameRef.GetHashCode()}");
            var thisWeekend2 = Period.ThisWeekend;
            Console.WriteLine(
                $"thisWeekend hash = {thisWeekend2.GetHashCode()}");

            Console.WriteLine(
                "thisWeekend == thisWeekendSameRef ?"
                + $"{thisWeekend == thisWeekendSameRef}");
            Console.WriteLine(
                "thisWeekend == thisWeekend2 ?"
                + $"{thisWeekend == thisWeekend2}");
            Console.WriteLine(
                "thisWeekend == today ?"
                + $"{thisWeekend == today}");

            var periodList = new List<Period>
            {
                Period.Random,
                Period.Random,
                Period.Random,
                Period.Random,
                Period.Random,
                Period.Random
            };
            periodList.Sort();
            foreach(var p in periodList)
                Console.WriteLine(p);
        }
    }
}
