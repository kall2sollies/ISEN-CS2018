﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library.Lessons
{
    public class I_Threads
    {
        public static void Run()
        {
            var myThread = new MyThreads();
            // 1er listener
            myThread.KeyPressed += MyThread_KeyPressed;
            // 2eme listener
            myThread.KeyPressed += args => {
                MyThreads.Log($"Lambda event listener heard {args.KeyPressed}", 5, ConsoleColor.Gray);
            };
            myThread.Run();
        }

        private static void MyThread_KeyPressed(KeyPressedEventArgs args)
        {
            MyThreads.Log($"OnMyThreadsKeyPressed event listener heard {args.KeyPressed}", 5, ConsoleColor.White);
        }
    }
}
