﻿using System;
using System.Collections.Generic;

namespace Isen.Cs.Library
{
    public interface IMyCollection<T>
    {
        /// <summary>
        /// Renvoie le nombre d'éléments 
        /// de la collection.
        /// </summary>
        int Count { get; }
        /// <summary>
        /// Renvoie le tableau primitif
        /// des valeurs de la collection
        /// </summary>
        T[] Values { get; }
        /// <summary>
        /// Renvoie ou affecte l'élément
        /// situé à l'index donné.
        /// </summary>
        /// <param name="index">index de l'élément</param>
        /// <returns>L'élément à l'index donné</returns>
        T this[int index] { get; set; }
        /// <summary>
        /// Renvoie le premier index de l'élément recherché,
        /// ou -1 s'il n'existe pas
        /// </summary>
        /// <param name="item">Elément à rechercher</param>
        /// <returns>index de l'élément</returns>
        int IndexOf(T item);
        /// <summary>
        /// Indique si la collection contient 
        /// l'élément
        /// </summary>
        /// <param name="item">Elément à chercher</param>
        /// <returns>vrai ou faux</returns>
        bool Contains(T item);
        /// <summary>
        /// Ajoute l'élément à la fin
        /// </summary>
        /// <param name="item"></param>
        void Add(T item);
        /// <summary>
        /// Insère l'élément à l'index donné
        /// </summary>
        /// <param name="index"></param>
        /// <param name="item"></param>
        void Insert(int index, T item);
        /// <summary>
        /// Retire l'élément à l'index donné
        /// </summary>
        /// <param name="index"></param>
        void RemoveAt(int index);
        /// <summary>
        /// Retire l'élément indiqué, ou
        /// le premier en cas de multiplicité
        /// </summary>
        /// <param name="item"></param>
        bool Remove(T item);
        /// <summary>
        /// Vide la collection
        /// </summary>
        void Clear();
        /// <summary>
        /// Renvoie l'interface d'énumération
        /// </summary>
        /// <returns></returns>
        IEnumerator<T> GetEnumerator();
    }
}