﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isen.Cs.Library
{
    public partial class Period
    {
        /// <summary>
        /// Début de la période (RW)
        /// </summary>
        public DateTime Start { get; private set; }
        /// <summary>
        /// Fin de la période (RW)
        /// </summary>
        public DateTime End { get; private set; }

        /// <summary>
        /// Durée de la période (R)
        /// </summary>
        public TimeSpan Length 
            => End - Start;
    }
}
