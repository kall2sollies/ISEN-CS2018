﻿using System;

namespace Isen.Cs.Library
{
    public static class DateTimeExtensions
    {
        public static bool IsBissextile(this DateTime dt)
        {
            //return DateTime.IsLeapYear(dt.Year);
            var march1st = new DateTime(dt.Year, 3, 1);
            return march1st.AddDays(-1).Day == 29;
        }

        public static DateTime LastMoment(
            this DateTime dt)
           => dt.Date.AddDays(1).AddTicks(-1);

        public static DateTime Next(
            this DateTime dt,
            DayOfWeek dw)
        {
            var cursor = dt.Date;
            while (true)
            {
                if (cursor.DayOfWeek == dw)
                    return cursor;
                cursor = cursor.AddDays(1);
            }
        }
    }
}