﻿using System;
using System.Collections.Generic;
using System.Text;
using Isen.Cs.Library;
using Xunit;

namespace Isen.Cs.Tests
{
    public class MyCollectionStringTests
    {
        [Fact]
        public void Count_Tests()
        {
            IMyCollection<string> myCollection = 
                new MyCollection<string>();
            Assert.True(myCollection.Count == 0);
            myCollection.Add("10");
            myCollection.Add("20");
            Assert.True(myCollection.Count == 2);
            myCollection.Remove("20");
            Assert.True(myCollection.Count == 1);
        }

        [Fact]
        public void Indexer_Tests()
        {
            IMyCollection<string> myCollection =
                new MyCollection<string>();
            var element0 = "10";
            var element1 = "20";
            var element2 = "30";
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 10 20 30
            Assert.True(myCollection[0] == element0);
            Assert.True(myCollection[1] == element1);
            Assert.True(myCollection[2] == element2);
            myCollection[0] = "A";
            myCollection[1] = "B";
            myCollection[2] = "C";
            Assert.True(myCollection[0] == "A");
            Assert.True(myCollection[1] == "B");
            Assert.True(myCollection[2] == "C");
        }

        [Fact]
        public void AddInsertRemove_Tests()
        {
            IMyCollection<string> myCollection =
                new MyCollection<string>();
            var element0 = "10";
            var element1 = "20";
            var element2 = "30";
            var elementInsert = "15";
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 10 20 30
            Assert.True(myCollection[0] == element0);
            Assert.True(myCollection[1] == element1);
            Assert.True(myCollection[2] == element2);

            myCollection.Insert(1, elementInsert);
            // 10 15 20 30
            Assert.True(myCollection[1] == elementInsert);
            Assert.True(myCollection[2] == element1);

            myCollection.Remove("15");
            // 10 20 30
            Assert.True(myCollection[2] == element2);
            Assert.True(myCollection.Count == 3);

            myCollection.RemoveAt(2);
            // 10 20
            Assert.True(myCollection.Count == 2);
            Assert.True(myCollection[1] == element1);
        }

        [Fact]
        public void IndexOf_Tests()
        {
            IMyCollection<string> myCollection =
                new MyCollection<string>();
            var element0 = "10";
            var element1 = "20";
            var element2 = "30";
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 10 20 30
            Assert.True(myCollection.IndexOf(element1) == 1);
            Assert.True(myCollection.IndexOf("sfdsfds") == -1);
        }

        [Fact]
        public void Enumerator_Tests()
        {
            IMyCollection<string> myCollection =
                new MyCollection<string>();
            var element0 = "10";
            var element1 = "20";
            var element2 = "30";
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 10 20 30

            using (var enumerator = myCollection.GetEnumerator())
            {
                // Par défaut le pointeur de l'énumérateur est "nulle part"
                var moveResult = enumerator.MoveNext();
                // 10 20 30
                //  ^      
                Assert.True(enumerator.Current == element0);
                Assert.True(moveResult);

                moveResult = enumerator.MoveNext();
                // 10 20 30
                //     ^  
                Assert.True(enumerator.Current == element1);
                Assert.True(moveResult);

                moveResult = enumerator.MoveNext();
                // 10 20 30
                //        ^  
                Assert.True(enumerator.Current == element2);
                Assert.True(moveResult);

                moveResult = enumerator.MoveNext();
                // 10 20 30
                //            ^  
                Assert.False(moveResult);
            } // à l'issue du bloc using, la variable enumerator est recyclée (Disposed)
        }
    }
}
