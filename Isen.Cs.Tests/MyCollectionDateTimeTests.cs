﻿using System;
using Isen.Cs.Library;
using Xunit;

namespace Isen.Cs.Tests
{
    public class MyCollectionDateTimeTests
    {
        [Fact]
        public void Count_Tests()
        {
            IMyCollection<DateTime> myCollection =
                new MyCollection<DateTime>();
            Assert.True(myCollection.Count == 0);
            myCollection.Add(new DateTime(2010, 1, 1));
            myCollection.Add(new DateTime(2020, 1, 1));
            Assert.True(myCollection.Count == 2);
            myCollection.Remove(new DateTime(2020, 1, 1));
            Assert.True(myCollection.Count == 1);
        }

        [Fact]
        public void Indexer_Tests()
        {
            IMyCollection<DateTime> myCollection =
                new MyCollection<DateTime>();
            var element0 = new DateTime(2010, 1, 1);
            var element1 = new DateTime(2020, 1, 1);
            var element2 = new DateTime(2030, 1, 1);
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 10 20 30
            Assert.True(myCollection[0] == element0);
            Assert.True(myCollection[1] == element1);
            Assert.True(myCollection[2] == element2);
            myCollection[0] = myCollection[0].AddMonths(1);
            myCollection[1] = myCollection[1].AddMonths(1);
            myCollection[2] = myCollection[2].AddMonths(1);
            Assert.True(myCollection[0].Month == 2);
            Assert.True(myCollection[1].Month == 2);
            Assert.True(myCollection[2].Month == 2);
        }

        [Fact]
        public void AddInsertRemove_Tests()
        {
            IMyCollection<DateTime> myCollection =
                new MyCollection<DateTime>();
            var element0 = new DateTime(2010, 1, 1);
            var element1 = new DateTime(2020, 1, 1);
            var element2 = new DateTime(2030, 1, 1);
            var elementInsert = new DateTime(2015, 1, 1);
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 2010 2020 2030
            Assert.True(myCollection[0] == element0);
            Assert.True(myCollection[1] == element1);
            Assert.True(myCollection[2] == element2);

            myCollection.Insert(1, elementInsert);
            // 2010 2015 2020 2030
            Assert.True(myCollection[1] == elementInsert);
            Assert.True(myCollection[2] == element1);

            myCollection.Remove(elementInsert);
            // 2010 2020 2030
            Assert.True(myCollection[2] == element2);
            Assert.True(myCollection.Count == 3);

            myCollection.RemoveAt(2);
            // 2010 2020
            Assert.True(myCollection.Count == 2);
            Assert.True(myCollection[1] == element1);
        }

        [Fact]
        public void IndexOf_Tests()
        {
            IMyCollection<DateTime> myCollection =
                new MyCollection<DateTime>();
            var element0 = new DateTime(2010, 1, 1);
            var element1 = new DateTime(2020, 1, 1);
            var element2 = new DateTime(2030, 1, 1);
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 2010 2020 2030
            Assert.True(myCollection.IndexOf(element1) == 1);
            Assert.True(myCollection.IndexOf(DateTime.MinValue) == -1);
        }

        [Fact]
        public void Enumerator_Tests()
        {
            IMyCollection<DateTime> myCollection =
                new MyCollection<DateTime>();
            var element0 = new DateTime(2010, 1, 1);
            var element1 = new DateTime(2020, 1, 1);
            var element2 = new DateTime(2030, 1, 1);
            myCollection.Add(element0);
            myCollection.Add(element1);
            myCollection.Add(element2);
            // 2010 2020 2030

            using (var enumerator = myCollection.GetEnumerator())
            {
                // Par défaut le pointeur de l'énumérateur est "nulle part"
                var moveResult = enumerator.MoveNext();
                // 10 20 30
                //  ^      
                Assert.True(enumerator.Current == element0);
                Assert.True(moveResult);

                moveResult = enumerator.MoveNext();
                // 10 20 30
                //     ^  
                Assert.True(enumerator.Current == element1);
                Assert.True(moveResult);

                moveResult = enumerator.MoveNext();
                // 10 20 30
                //        ^  
                Assert.True(enumerator.Current == element2);
                Assert.True(moveResult);

                moveResult = enumerator.MoveNext();
                // 10 20 30
                //            ^  
                Assert.False(moveResult);
            } // à l'issue du bloc using, la variable enumerator est recyclée (Disposed)
        }
    }
}